//! Super simple dice roller
mod roll;

use std::env;
use roll::Roll;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() <= 1 {
        eprintln!("No rolls given");
        return;
    }

    let mut roll_list: Vec<Roll> = Vec::new();

    // Parsing rolls
    for r in args.iter().skip(1) {
        match Roll::parse_roll(r) {
            Ok(roll) => {
                roll_list.push(roll);
            },
            Err(err) => {
                eprintln!("{err}");
            }
        }
    }

    // Rolling rolls
    for r in roll_list.iter_mut() {
        if let Err(err) = r.roll_and_save() {
            eprintln!("{err}");
        }
    }

    // Printing rolls
    for r in roll_list.iter() {
        r.show_roll();
    }
}
