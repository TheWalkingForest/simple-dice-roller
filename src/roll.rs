use rand::Rng;

#[derive(Debug)]
pub struct Roll {
    pub size: u8,
    pub amount: u8,
    pub explode: u8,
    pub value: u8,
    pub list: Vec<u8>,
    pub valid: bool,
}

impl Roll {
    /// Creates an empty roll, equivilent to 0d0e0
    pub fn new() -> Roll {
        Roll::from_parts(0, 0, 0)
    }

    /// Creates a roll from given input ('amount'd'size'e'explode')
    pub fn from_parts(size: u8, amount: u8, explode: u8) -> Roll {
        Roll {
            size,
            amount,
            explode: if explode != 0 { explode } else { 0 },
            value: 0,
            list: Vec::new(),
            valid: size > 0 && size >= explode,
        }
    }

    /// A nice way to show what a roll is
    pub fn show_roll(&self) {
        if self.valid {
            if self.explode != 0u8 {
                println!(
                    "Rolling a d{} {} time(s), exploding on {}",
                    self.size, self.amount, self.explode
                );
            } else {
                //println!("Rolling a d{} {} time(s)", self.size, self.amount);
                println!("Rolling a d{} {} time(s)", self.size, self.amount);
            }
        }
        else {
            eprintln!("ERROR: {self:?} is an invalid roll");
        }
    }

    /// Checks if a roll is equal to self
    pub fn equals(&self, rhs: Roll) -> bool {
        self.size == rhs.size &&
        self.amount == rhs.amount &&
        self.explode == rhs.explode
    }

    pub fn roll_and_save(&mut self) -> anyhow::Result<u8> {

        if !self.valid  {
            return Err(anyhow::anyhow!("ERROR: trying to roll invalid roll"));
        }

        let mut sum = 0;
        let mut rng = rand::thread_rng();

        for _ in 0..self.amount {
            loop {
                let num: u8 = rng.gen_range(0..self.size) + 1;
                self.list.push(num);
                sum += num;

                if self.explode != 0 || num < self.explode {
                    break;
                }
            }
        }

        self.value = sum;

        Ok(sum)
    }

    pub fn roll(&self) -> anyhow::Result<u8> {

        if !self.valid  {
            return Err(anyhow::anyhow!("ERROR: trying to roll invalid roll"));
        }

        let mut sum = 0;
        let mut rng = rand::thread_rng();

        for _ in 0..self.amount {
            loop {
                let num: u8 = rng.gen_range(0..self.size) + 1;
                sum += num;

                if self.explode != 0 || num < self.explode {
                    break;
                }
            }
        }

        Ok(sum)

    }

    /// Checks if the given string is a valid roll format
    pub fn valid_roll(given: &str) -> bool {
        let re = lazy_regex::regex!("^[0-9]+d[0-9]+(e[0-9]+)?$");
        re.is_match(given)
    }

    /// Gets size of dice and number of rolls
    pub fn parse_roll(set: &str) -> anyhow::Result<Roll> {
        if Roll::valid_roll(set) {
            let roll: Vec<_> = set.split(|c| c == 'd' || c == 'e').collect();

            let rolls = roll[0].parse::<u8>().unwrap();
            let dice = roll[1].parse::<u8>().unwrap();

            let exp = || -> u8 {
                if roll.len() == 3 {
                    roll[2].parse::<u8>().unwrap()
                } else {
                    0u8
                }
            };
            Ok(Roll::from_parts(dice, rolls, exp()))
        } else {
            Err(anyhow::anyhow!("ERROR: {set} Invalid roll"))
        }
    }
}

impl Default for Roll {
    fn default() -> Self {
        Roll::new()
    }
}
