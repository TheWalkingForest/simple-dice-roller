# Simple Dice Roller

## Purpose
This was created as a simple project to start learnig the Rust language as well as to maybe be useful for those who may need a simple dice roller.

## Usage
To use the simple-dice-roller, just give it any amount of rolls.  For example, to roll three(3) six(6) sided dice, you would put it in the format `3d6`.

However, sometimes you may want to roll again if a certain number or higher is rolled.  This is called exploding dice.  To add this to a roll, simply add `e` followed by the minimum value to explode on.  Using the previous roll, we now want the roll to explode on values higher than five(5).  The new roll would be `3d6e5`.

## Requirements

Currently, only Linux and MacOS are supported.

* cargo - Rust's package manager
* [just](https://github.com/casey/just) - (soft requirement) a makefile clone written in rust

## Installation

1. Clone repo `https://gitlab.com/TheWalkingForest/simple-dice-roller.git`
2. Build project
    * `just build` if using just
    * `cargo build` if using cargo
3. Run with `cargo run [rolls]`
4. Using `just link`, you can sym link the binary to `~/.local/bin`
5. After sym linking and if `~/.local/bin` is in your path, you can run with `simple-dice-roller [rolls]` from anywhere

## License

The code in this repository are licensed under the GNU GPLv2 License.